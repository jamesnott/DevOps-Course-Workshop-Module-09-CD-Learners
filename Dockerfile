FROM mcr.microsoft.com/dotnet/sdk:6.0
RUN curl -fsSL https://deb.nodesource.com/setup_19.x | bash - && apt-get install -y nodejs
COPY . /app
WORKDIR /app
RUN dotnet build
WORKDIR /app/DotnetTemplate.Web/
RUN npm install && find ./node_modules/ ! -user root | xargs chown root:root
RUN npm run build
ENTRYPOINT ["dotnet","run"]